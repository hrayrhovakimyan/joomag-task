<?php

/**
 * Class Error
 * made for showing some error pages dependent on situation
 */
class Error{
    public static function throw404(){
        die("<p style='font-size: 40px; font-weight: bold;'>Page Not found</p>");
    }

    public static function throw500(){
        die("<p style='font-size: 40px; font-weight: bold;'>Internal Server Error</p>");
    }

    public static function throw403(){
        die("<p style='font-size: 40px; font-weight: bold;'>Access denied</p>");
    }
}