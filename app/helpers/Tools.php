<?php

/**
 * Class Tools
 * designed for serving as tool for doing some operations which can not be included in any other
 * class of Prject
 */
class Tools{

    /**
     * @param $url
     * @return array
     * explodes pretty url and returns its parts
     */
    public static function parseUrl($url){
        if (empty($url)){
            $url_parts = ["", ""];
        }
        else{
            $exploded_url = explode('/', ltrim($url, '/'));
            $action = $exploded_url[0];
            $method = isset($exploded_url[1]) ? $exploded_url[1] : "";
            $url_parts = [$action, $method];
        }
        return $url_parts;
    }

    /**
     * @param $view
     * @param array $data
     * includes view file and passes data to it optionally
     */
    public static function view($view, $data=[]){
        /**
         * $data is expected to be assoc array, and assoc array keys are transformed to variables
         * setting their values as $variable content
         */
        foreach ($data as $key => $value){
            $$key = $value;
        }

        if (file_exists(VIEWS_FOLDER.$view.".php")){
            require_once(VIEWS_FOLDER.$view.".php");
        }
        else{
            require_once(VIEWS_FOLDER."login.php");
        }
    }

    /**
     * @param $error -  response status
     * @param $response_key
     * @param $data
     * json encodes response and echos it to client
     */
    public static function responseAjax($error, $response_key, $data){
        $response = [
            'error'=>$error,
        ];

        echo json_encode($response);
        die;
    }

    /**
     * @param int $length
     * @return string
     * generates random string with given length
     */
    public static function generateRandomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param $password
     * @return string
     * Hashes the clean password and returns hashed value
     * Sorry for using md5 I know it is broken, but I used it to save time
     */
    public static function hashPassword($password){
        return md5($password);
    }




}