<?php

/**
 * Class DBConnection
 * Designed for connecting with database in one class
 */
class DBConnection{
    /**
     * @var PDO
     * connection object
     */
    private static $con;

    /**
     * database credential constants
     */
    const HOST = "contactsapp.dev";
    const USERNAME = "root";
    const PASSWORD = "";
    const DB_NAME = "contactsapp";
    const CHARSET = "utf8";

    /**
     * DBConnection constructor.
     * Makes new connection to database if there was no other connection, else returns already established connection
     * Singleton pattern
     */
    public function __construct(){
        if (!self::$con){
            $dsn = "mysql:dbname=".self::DB_NAME.";host=".self::HOST.";charset=".self::CHARSET;
            try {
                self::$con = new PDO($dsn, self::USERNAME, self::PASSWORD);
                /**
                 * sets attribute PDO::ATTR_ERRMODE for catching pdo exceptions not giving fatal errors
                 */
                self::$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return self::$con;
            } catch (PDOException $e) {
                /**
                 * Ends immediately if there was no connection to database
                 */
                Error::throw500();
            }
        }
        else{
            return self::$con;
        }
    }

    /**
     * @param $sql - sql query to be executed
     * @param bool $params - params for bounding to prepared sql queries
     * @param bool $single - flag to determine if single result needs to be fetched or not
     * @param int $fetch_mode - PDO constant showing the fetch mode
     * @return array|mixed|null returns data from database for select queries, returns null for non select queries
     *
     */
    public function query($sql, $params=false, $single = false, $fetch_mode = PDO::FETCH_ASSOC){
        if (!$params){
            $stmt = self::$con->query($sql);
        }
        else{
            $stmt = self::$con->prepare($sql);
            foreach ($params as $param){
                if (!isset($param['type'])) $param['type'] = PDO::PARAM_STR;
                $stmt->bindParam($param['key'], $param['value'], $param['type']);
            }
            $stmt->execute();
        }

        if ($fetch_mode !== false){
            if ($single){
                $single_result = $stmt->fetch($fetch_mode);
                return $single_result;
            }
            else{
                return $stmt->fetchAll($fetch_mode);
            }
        }
        return NULL;
    }

    /**
     * @param $sql
     * @throws Exception and rolls transaction back if some failure happened
     * executes some huge non sql queries with transactions
     */
    public function queryWithTransaction($sql){
        try {
            self::$con->beginTransaction();
            self::$con->exec($sql);
            self::$con->commit();
        } catch (Exception $e) {
            self::$con->rollBack();
            throw new Exception("Failed: " . $e->getMessage());
        }
    }
}