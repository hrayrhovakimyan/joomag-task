<?php

/**
 * Class FormValidator
 * Designed to validate form data(urls, emails, usernames, etc)
 * sub class of Validator class
 */
class FormValidator extends Validator{

    /**
     * @param $rules
     * @param $data
     * extended from parent class
     * applies rule for specified key data
     */
    public function validate($rules, $data){
        parent::validate($rules, $data);
        foreach ($this->data as $key => $dataItem){

            /**
             * filters string data
             */
            if (!is_numeric($dataItem) && is_string($dataItem)){
                $dataItem = Filter::escapeInput($dataItem);
            }

            $rule = $this->rules[$key];
            if (isset($rule['required'])){
                if ($rule['required']){
                    $this->checkEmpty($key, $dataItem);
                }
            }

            if (isset($rule['type'])){
                $method_name = "check".ucfirst($rule['type']);
                $this->{$method_name}($dataItem);
            }

            if (isset($rule['length'])){
                $this->checkLength(ucfirst($key), $dataItem,$rule['length']);
            }
        }
    }

    /**
     * @param $key
     * @param $data
     * $return void
     * Checks if $data is empty or not if empty add error
     */
    private function checkEmpty($key, $data){
        if (empty($data)){
            $this->addError($key." is empty");
        }
    }

    /**
     * @param $key
     * @param $data
     * @param $rule - contains min and/or max length
     * check string length
     */
    private function checkLength($key, $data,$rule){
        $str_length = strlen($data);
        if (isset($rule['min'])){
            if ($str_length<$rule['min']){
                $this->addError("{$key} {$data} min length must be {$rule['min']} characters");
            }
        }
        if (isset($rule['max'])){
            if ($str_length > $rule['max']){
                $this->addError("{$key} {$data} max length must be {$rule['max']} characters");
            }
        }
    }

    /**
     * @param $string
     * checks email
     */
    private function checkEmail($string){
        if (filter_var($string, FILTER_VALIDATE_EMAIL) === false) {
            $this->addError("$string is not a valid email address ");
        }
    }

    /**
     * @param $string
     * checks phone number with regex
     */
    private function checkPhone($string){
        if (!preg_match("/[0-9\+\-()]/", $string)){
            $this->addError($string." is not valid phone ");
        }
    }

    /**
     * @param $string
     * checks $string to be word(consist from alphanumeric characters - for firstname, lastname, etc)
     */
    private function checkAlpha($string){
        if (!preg_match("/[A-z]+/", $string)){
            $this->addError($string."is not valid word");
        }
    }

    /**
     * @param $string
     * checks $string to be valid url
     */
    private function checkUrl($string){
        if (filter_var($string, FILTER_VALIDATE_URL) === false) {
            $this->addError("$string is not valid url");
        }
    }
}