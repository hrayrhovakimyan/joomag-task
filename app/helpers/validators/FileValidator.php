<?php

/**
 * Class FileValidator
 * Designed to validate files
 * sub class of Validator class
 */
class FileValidator extends Validator{

    /**
     * @param $rules
     * @param $data
     * extended from Validator
     */
    public function validate($rules, $data){
        parent::validate($rules, $data);
        foreach ($this->data as $key => $file){
            $rule = $this->rules[$key];

            if (isset($rule['file_base'])){
                if ($rule['file_base'] == "upload"){
                    $this->checkUploadFileKeys($file);
                }
            }

            if (isset($rule['allowed_types'])){
                $this->checkAllowedTypes($file["name"], $rule['allowed_types']);
            }

            if (isset($rule['size'])){
                $this->checkSize($file["size"], $rule['size']);
            }

        }
    }

    /**
     * @param $file
     * if file is uploaded rom client checks if all keys of $_FILES exist, they can be missed by some hacker trick
     */
    private function checkUploadFileKeys($file){
        $array_keys = array_keys($file);
        $array_diff = array_diff($array_keys, ['size','name','error', 'tmp_name','type']);
        if (!empty($array_diff)){
            $this->addError('Not valid file');
        }
    }

    /**
     * @param $file_name
     * @param $allowed_types
     * checks file extension to match allowed types
     */
    private function checkAllowedTypes($file_name, $allowed_types){
        $ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
        if (!in_array($ext, $allowed_types)){
            $this->addError("Please upload {$ext} file");
        }
    }

    /**
     * @param $file_size
     * @param $rule
     * Checks file size(in MBs)
     */
    private function checkSize($file_size, $rule){
        if ($file_size > $rule * 1024 * 1024){
            $this->addError("Max file upload size is {$rule}MB");
        }
    }

}