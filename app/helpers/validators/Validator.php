<?php

/**
 * Class Validator
 * designed to manage validation
 */
class Validator{
    /**
     * @var $data as array to be validated
     * @var $rules as array for validation
     * @var $errors - string for collecting errors
     */
    protected $data, $rules, $errors = "";

    /**
     * @param $rules
     * @param $data
     * validates the data with rules
     * is extended and used for sub classes with their rules
     */
    public function validate($rules, $data){
        $this->rules = $rules;
        $this->data = $data;
    }

    /**
     * @return bool
     * returns if data is validated or not
     */
    public function isValid(){
        return empty($this->errors) ? true : false;
    }

    /**
     * @return string
     * return errors of validation
     * is called when isValid returns false
     */
    public function getErrors(){
        return rtrim($this->errors, "<br>");
    }

    /**
     * @param $msg
     * Adds error tp $errors
     */
    protected function addError($error){
        $this->errors .= $error."<br>";
    }
}
?>
