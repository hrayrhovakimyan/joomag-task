<?php
session_start();

/**
 * Class SessionManager
 * designed for working with Sessions easier
 */
class SessionManager{

    /**
     * @param $key
     * @param $value
     * create session
     */
    public static function create($key, $value){
        $_SESSION[$key] = $value;
    }

    /**
     * @param $key
     * unset session by key
     */
    public static function remove($key){
        if (isset($_SESSION[$key])){
            unset($_SESSION[$key]);
        }
    }

    /**
     * @param $key
     * @return bool
     * @throws Exception
     * get session value by key
     */
    public static function get($key){
        if (isset($_SESSION[$key])){
            return $_SESSION[$key];
        }
        return false;
    }

    /**
     * Destroys all sessions
     */
    public static function removeAll(){
        session_destroy();
    }

}