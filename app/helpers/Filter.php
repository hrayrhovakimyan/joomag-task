<?php

/**
 * Class Filter
 * used for filtering data
 */
class Filter
{
    /**
     * @param $str
     * @return string
     * trims input, strips tags and converts all applicable characters to HTML entities
     */
    public static function escapeInput($str){
        return htmlentities(strip_tags(trim($str)));
    }

    /**
     * @param $url
     * @return string
     * rtrims url and converts all applicatble characters to HTML entities for XSS prevention
     */
    public static function escapeUrl($url){
        return htmlentities(rtrim($url, '/'));
    }

}