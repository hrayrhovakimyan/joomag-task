<?php

/**
 * Class Contact
 * Designed to manipulate contacts table data
 */
class Contact{

    /**
     * @var DBConnection
     * @var FileValidator
     * @var FormValidator
     * and some contants for later user
     */
    private $dbHandle, $file_validator, $form_validator;
    const CLIENT_GENERAL_ERR_MSG = "File upload failed";
    const CLIENT_INVALID_CSV_MSG = "CSV file is invalid";

    /**
     * Contact constructor.
     * creates instances of DBConnection, FileValidator, FormValidator classes
     */
    public function __construct(){
        $this->dbHandle = new DBConnection();
        $this->file_validator = new FileValidator();
        $this->form_validator = new FormValidator();
    }

    /**
     * @param $user_id
     * @return array|mixed|null
     * Retrieves user contacts from database, NULL of nothing found
     */
    public function getUserContacts($user_id){
        $sql = "SELECT firstname, lastname, phone, email FROM contacts WHERE user_id = :user_id";
        $contact_data = [
            ['key'=>':user_id', "value"=>$user_id, 'type'=>PDO::PARAM_INT ],
        ];
        $result = $this->dbHandle->query($sql, $contact_data);
        return $result;
    }

    /**
     * @param $file
     * @param $user_id
     * @return array
     * import contact list from csv file to db for specified user
     */
    public function importCSV($file, $user_id){

        /**
         * Validates the file
         */
        $rules = [
            'file'=>[
                'file_base'=>'upload',
                'allowed_types'=>['csv'],
                'max_size'=>2,
            ]
        ];

        $this->file_validator->validate($rules, ['file'=>$file]);
        $file_valid = $this->file_validator->isValid();
        if (!$file_valid){
            return [
                'error'=>1,
                'data'=>self::CLIENT_INVALID_CSV_MSG
            ];
        }


        /**
         * if file valid gets file's content no need for upload csv file to server
         */
        $fileContent = file_get_contents($file['tmp_name']);

        /**
         * if file_get_contents function succeeds then process CSV and get data for inserting in db as part of query
         * else returns error
         */
        if ($fileContent){
            $result = $this->processContactCSV($fileContent, $user_id);
            if ($result['error'] == 0){
                $dbData = $result['data'];
            }
            else{
                return [
                    'error'=>1,
                    'data'=>self::CLIENT_INVALID_CSV_MSG
                ];
            }
        }
        else{
            return [
                'error'=>1,
                'data'=>self::CLIENT_INVALID_CSV_MSG
            ];
        }

        /*
         * Insert CSV data to contacts table
         * After fetching data checks if it is not empty then
         * Inserts it to database with transaction to provide query speed, else fails
         */
        if (!empty($dbData)){
            try{
                $dbData = rtrim($dbData, ",");
                $sql = "INSERT INTO contacts (user_id, firstname, lastname, phone, email) VALUES".$dbData;
                $this->dbHandle->queryWithTransaction($sql);
                return [
                    'error'=>0,
                    'data'=>'Success'
                ];
            }
            catch(Exception $e){
                return [
                    'error'=>1,
                    'data'=>self::CLIENT_GENERAL_ERR_MSG
                ];
            }
        }
    }

    /**
     * @param $fileContent
     * @param $user_id
     * @return array
     * Processes csv and returns database string
     */
    private function processContactCSV($fileContent, $user_id){
        $result = "";
        /**
         * divides file content to array of rows by exploding
         */
        $fileRows = explode("\n", $fileContent);

        /**
         * if file is empty, there is only one row
         * returns error
         */
        if (empty($fileRows) || count($fileRows) == 1){
            return [
                'error'=>1,
                'data'=>self::CLIENT_GENERAL_ERR_MSG
            ];
        }

        /**
         * Validation rules for csv columns
         */
        $fileContentValidationRules = [
            'firstname'=>[
                'length'=>[
                    'min'=>2
                ],
                'type'=>'alpha'
            ],
            'lastname'=>[
                'length'=>[
                    'min'=>2
                ],
                'type'=>'alpha'
            ],
            'phone'=>[
                'type'=>'phone'
            ],
            'email'=>[
                'type'=>'email'
            ],
        ];

        /**
         * Fetching csv header from file, if no valid data returns error
         */
        $csv_header = explode(',', $fileRows[0]);
        if (!empty($csv_header)){
            /*
             * exchanges keys and values of $csv_header array
             * array_flip used for later fetching of csv header column position
             * it generates something like this
             * [
             *      'firstname'=>0,
             *      'lastname' => 1,
             *      'phone' => 2
             *      'email' => 3
             * ]
             */
            $csv_header = array_flip($csv_header);
        }
        else{
            return [
                'error'=>1,
                'data'=>self::CLIENT_GENERAL_ERR_MSG
            ];
        }

        /**
         * removes csv header from rows array
         */
        array_shift($fileRows);

        /**
         * iterate over data containing rows
         */
        foreach ($fileRows as $row){
            $row_data = explode(",", $row);
            /**
             * explodes each row to columns
             * if there are more than 4 columns, which is not right there must be only 4 in each row(Firstname lastname, email, phone)
             */
            if (count($row_data) > 4){
                Tools::responseAjax(1, 'data', 'Invalid CSV File ');
            }
            else if (count($row_data) < 4){
                /*
                 * For escaping empty rows
                 */
                continue;
            }

            /**
             * stores row data in object where values are formatted to be normal with
             * csv column reordering
             */
            $data = [
                'firstname'=>$row_data[$csv_header['firstname']],
                'lastname'=>$row_data[$csv_header['lastname']],
                'phone'=>$row_data[$csv_header['phone']],
                'email'=>$row_data[$csv_header['email']]
            ];

            $this->form_validator->validate($fileContentValidationRules,$data);

            /**
             * if one of rows is invalid continues to next row
             */
            if (!$this->form_validator->isValid()){
                continue;
            }

            /**
             * if data is valid, concats values to be in form of database string to be concated ater VALUES statement
             */
            $result .= "('".$user_id."', '".$data['firstname']."', '".$data['lastname']."', '".$data['phone']."', '".$data['email']."'), ";
        }

        $result = rtrim($result, ", ");

        return [
            'error'=>0,
            'data'=>$result
        ];
    }



}