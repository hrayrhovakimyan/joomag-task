<?php

/**
 * Class User
 * Model for manipulating user data
 */
class User{

    /**
     * @var DBConnection object
     */
    private $dbHandle, $email, $password, $form_validator;

    /**
     * User constructor.
     * @param null $email
     * @param null $password
     * Email and password can be passed(for example in login process)
     */
    public function __construct($email = NULL, $password = NULL){
       $this->email = $email;
       $this->password = $password;
       $this->dbHandle = new DBConnection();
       $this->form_validator = new FormValidator();
    }

    /**
     * @return current user id or NULL based on Session value
     */
    public function getCurrentUserId(){
        $session = SessionManager::get('user');
        $query = "SELECT id FROM users WHERE session = :session_string";
        $data = [
            ['key'=>':session_string', "value"=>$session ]
        ];
        $result = $this->dbHandle->query($query, $data, true);
        if ($result){
            return $result['id'];
        }
        else{
            return null;
        }
    }

    /**
     * @return bool
     * tries to login the user
     * hashes password, makes a query to db to define is there such user or not
     */
    public function login(){
        $validation_rules = [
            'email'=>[
                'required'=>true,
                'type'=>'email'
            ],
            'password'=>[
                'required'=>true,
                'length'=>[
                    'min'=>6,
                    'max'=>20
                ]
            ]
        ];

        $this->form_validator = new FormValidator($validation_rules, ['email'=>$this->email, 'password'=>$this->password]);
        $errors = $this->form_validator->isValid();

        if (!$errors){
            return [
                'error'=>1,
                'data'=>$errors
            ];
        }

        $this->password = Tools::hashPassword($this->password);
        $login_query = "SELECT id FROM users WHERE email = :email AND password = :password";
        $login_data = [
            ['key'=>":email", 'value'=>$this->email],
            ['key'=>":password", 'value'=>$this->password],
        ];

        /**
         * Passing sql and future bounded data to function
         */
        $result = $this->dbHandle->query($login_query, $login_data, true);
        if (!$result){
            return [
                'error'=>1,
                'data'=>'Wrong email, password'
            ];
        }
        else{
            /**
             * If there is such user in db, creates session for him ans saves session to db
             */
            $user_id = $result['id'];
            try{
                $this->setUserSessionId($user_id);
                return [
                    'error'=>0,
                    'data'=>'Success'
                ];
            }
            catch(Exception $e){
                return [
                    'error'=>1,
                    'data'=>'Wrong email, password'
                ];
            }


        }
    }

    /**
     * @param $user_id
     * Sets session and saves session in db for specified user id
     */
    private function setUserSessionId($user_id){
        $session = Tools::generateRandomString();
        SessionManager::create("user", $session);

        $session_save_query = "UPDATE users SET session = :session_string WHERE id = :user_id";
        $data = [
            ['key'=>":session_string", 'value'=>$session],
            ['key'=>":user_id",        'value'=>$user_id],
        ];
        $this->dbHandle->query($session_save_query, $data, false, false);
    }

    /**
     * unsets user session in database
     * and redirects user to login page
     */
    public function logout(){
        $current_session = SessionManager::get("user");
        $session_unset_query = "UPDATE users SET session = NULL WHERE session = :session_string";
        $data = [
            ['key'=>":session_string", 'value'=>$current_session],
        ];
        $this->dbHandle->query($session_unset_query, $data, false, false);
    }



}