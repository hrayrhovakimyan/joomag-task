<?php
/*
 * Inits application by sending appropriate request to appropriate controller
 */
class App {


    public static function init(){
        /*
         * Require autoload for autoloading all classes
         */
        require_once APP_FOLDER."autoload.php";


        /*
         * Filter request url for escaping html entities and triming last slash
         */
        $request_url = Filter::escapeUrl($_SERVER['REQUEST_URI']);


        /*
         * if $request_url is empty send user to login page
         */
        if (empty($request_url)){
            //show login page
            self::callDefault();
            return false;
        }

        /*
         * Get url parts splited by slash
         * Only 2 parts are accepted
         */
        list($fp, $sp) = Tools::parseUrl($request_url);

        /**
         * If only one param specified
         * send request to specific controller depend on $fp
         */
        if (empty($sp)){
            if (in_array($fp, ["login", "logout"])){
                new Login($fp, $_SERVER['REQUEST_METHOD']);
            }
            else if ($fp="contacts"){
                new Contacts($fp, "", $_SERVER['REQUEST_METHOD']);
            }
            else{
                /*
                 * If no match for $fp show 404 error page
                 */
                Error::throw404();
            }
        }
        else if ($fp="contacts"){
            /*
             * Cause in our application only Contacts controller can have $sp, directly send request to it
             */
            new Contacts($fp, $sp, $_SERVER['REQUEST_METHOD']);
        }
    }


    /*
     * void function
     * returns login page
     */
    private static function callDefault(){
        $login = new Login("login", "GET");
        $login->index();
    }

}