<?php

class Login{

    /**
     * @var FormValidator
     */
    private $form_validator;

    /**
     * Login constructor.
     * @param $action
     * @param $request_method
     * Based on request and request method, decides what action to do if allowed
     */
    public function __construct($action, $request_method){
        if ($action == "login"){
            if ($request_method == "GET"){
                $this->index();
            }
            else{
                $this->login();
            }
        }
        else if ($action == "logout" && $request_method == "GET"){
            $this->logout();
        }
        else{
            Error::throw403();
        }
    }

    /**
     * Just shows the login form
     */
    public function index(){
        Tools::view('login');
    }

    /**
     * validates user input and logs user in
     */
    private function login(){
        if (isset($_POST['email'], $_POST['password'])){
            $email = $_POST['email'];
            $password = $_POST['password'];
            $user = new User($email, $password);
            $login = $user->login();
            if ($login['error'] == 0){
                Tools::responseAjax(0, 'data', 'Success');
            }
            else{
                Tools::responseAjax(1, 'data', $login['data']);
            }
        }
        else{
            Tools::responseAjax(1, "data", "All fields required");
        }
    }

    private function logout(){
        $user = new User;
        $user->logout();
        header("Location: ".DOMAIN."/login");
    }

}