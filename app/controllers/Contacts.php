<?php

/**
 * Class Contacts
 * Contacts controller
 */
class Contacts{

  private $user_id;

    /**
     * Contacts constructor.
     * @param $class
     * @param $method
     * @param $request_type
     * takes the request and decides whether user can make such request looking also at $request_type
     */
  public function __construct($class, $method, $request_type){
      /**
       * checks if user is authorized
       * if yes continues else redirects to login page
       */
    $user = new User;
    $this->user_id = $user->getCurrentUserId();
    if ($this->user_id){
        if ($class == "contacts"){
          if ((empty($method) || $method == "lists") && $request_type == "GET"){
            $this->index();
          }
          else if ($request_type == "POST" && $method == "import"){
            $this->import();
          }
          else{
            Error::throw403();
          }
        }
        else{
          Error::throw403();
        }
    }
    else{
        header("Location: ".DOMAIN);
    }

  }

    /**
     * index() function which can be managed with /contacts or /contacts/lists route
     * takes contacts from model and shows it in contacts view
     */
  public function index(){
    $contact_model = new Contact();
    $contacts = $contact_model->getUserContacts($this->user_id);
    Tools::view("contacts", ['contacts'=>$contacts]);
  }

    /**
     * Gives model the file to upload and delivers response to the
     * client
     */
  public function import(){
     if (isset($_FILES["csv_file"]) && is_array($_FILES["csv_file"]) && count($_FILES["csv_file"]) == 5){
        $contacts_m = new Contact();
        $data = $contacts_m->importCSV($_FILES["csv_file"], $this->user_id);
        Tools::responseAjax($data['error'], 'data', $data['data']);
     }
     else{
        Tools::responseAjax(1, 'data', 'No File uploaded');
     }
  }

}