<?php
/*
 * Auto loads all classes
 * tries to find classes in all app/ folders
 * on failure throws an exception and stops the program
 */

require_once "helpers/Error.php";

function __autoload($class) {
    $class_folders = [
        APP_FOLDER."controllers".DIRECTORY_SEPARATOR,
        APP_FOLDER."helpers".DIRECTORY_SEPARATOR,
        APP_FOLDER."helpers".DIRECTORY_SEPARATOR."validators".DIRECTORY_SEPARATOR,
        APP_FOLDER."models".DIRECTORY_SEPARATOR,
    ];

    $required = false;
    if ($class == "Error"){
        $required = true;
    }
    else{
        foreach ($class_folders as $folder){
            if (file_exists($folder.$class.".php")){
                require_once $folder.$class.".php";
                $required = true;
                break;
            }
        }
    }

    if (!$required){
        Error::throw500();
    }
}