To run this project you need PHP version > 5.3 < 7, APACHE, MYSQL(using xampp)
After git pull
1. create a virtual host and add it to apache/conf/extra/httpd-vhosts.conf
<VirtualHost contactsapp.dev:80>
    DocumentRoot "C:/{Your Path}contactsapp/public"
    ServerName contactsapp.dev
</VirtualHost>
2. For Windows users - add <contactsapp.dev> this domain to your Windows hosts files
3. Create database with name contactsapp
4. Import demo-database.sql there
5. Change mysql username, password to your system's from contactsapp\app\helpers\DBConnection.php
6. Demo database is included in project you can login then(all passwords are 123456, writing these because passwords are hashed)
7. Mock data csv file is also included in project to have some test data

Project is using MVC architecture, so there are models, controllers , views in project, also there are some assets(css, js files)
Everything starts from public folder index.php file(routing configured with .htaccess)
Later every file docs are in those files