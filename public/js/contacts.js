$(document).ready(function(){

    var import_csv_form = $("#csv-form");

    /**
     * Bootstrap datatables used to display contact data
     */
    $("#contacts-list").DataTable();

    /**
     * on import btn clicked
     * show upload file dialog
     */
    $(".import-btn").click(function(ev){
        ev.preventDefault();
        $("#csv_file").trigger("click");
    });

    /**
     * on file upload submit the form
     * Show progress before form submission completed
     */
    $(document).on("change", "#csv_file", function(){
        var uploaded_file, ext;
        $(import_csv_form).ajaxSubmit({
            url:"/contacts/import",
            type: "POST",
            target:$("#contacts-list"),
            dataType:  'json',
            clearForm: true,
            beforeSubmit:function(arr, $form, options){
                uploaded_file = arr[0].value;
                ext = uploaded_file["name"].split('.').pop().toLowerCase();
                if (ext != "csv"){
                    alert("Please upload an csv file");
                    return false;
                }

                if (uploaded_file["size"] > 2 * 1024 * 1024){
                    alert("Max file upload size is 2MB");
                    return false;
                }

                $(".page-load-spinner").show();
            },
            success:function(response, statusText, xhr){
                $(".page-load-spinner").hide();
                if (response.error == "0"){
                    location.reload();
                }
                else{
                    $(".form-error").text(response.data);
                }
            },
            error:function(){
                $(".page-load-spinner").hide();
            }
        })
    });
});