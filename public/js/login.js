$(document).ready(function(){

    var login_form = $("#login-form").first();

    login_form.submit(function(evt){
        evt.preventDefault();
        var email = $("#email").val();
        var password = $("#password").val();
        if (email != "" && password != ""){

            if (!isEmail(email)){
                $(".form-error").text("Email is not valid").show();
                return false;
            }

            if (!isValidPassword(password)){
                $(".form-error").text("Password must be from 6 to 20 characters").show();
                return false;
            }

            $(".form-error").hide();

            $.ajax({
                method:"POST",
                url:"/login",
                dataType:"json",
                data:{
                    email:email,
                    password:password
                },
                success: function (msg) {
                    if (msg.error == "1"){
                        $(".form-error").html(msg.data).show();
                    }
                    else if (msg.error == "0"){
                        location.href = "/contacts";
                    }
                },
                error:function(xhr, status, statusText){
                    $(".form-error").text("Something went wrong").show();
                }
            })
        }
        else{
            $(".form-error").text("Fill in email and password").show();
        }
    });
});