<?php

/*
 * defining some constants for later user
 */
define("VIEWS_FOLDER", __DIR__ . DIRECTORY_SEPARATOR ."views".DIRECTORY_SEPARATOR);
define("APP_FOLDER", str_replace("public","",__DIR__) . DIRECTORY_SEPARATOR . "app". DIRECTORY_SEPARATOR);
define("UPLOADS_FOLDER", __DIR__ . DIRECTORY_SEPARATOR . "uploads". DIRECTORY_SEPARATOR);
define("DOMAIN", "http://contactsapp.dev");

/*
 * Init application with calling App controller
 */
require_once "../app/controllers/App.php";
App::init();