<!Doctype html>
<html>
    <head>
        <title>Contact App</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../css/main.css">
        <script type="text/javascript" src="../../lib/jquery.js"></script>
        <script type="text/javascript" src="../../lib/bootstrap/js/bootstrap.min.js"></script>