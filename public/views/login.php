<?php
require_once("partials/head.php");
?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <h1 class="text-center text-capitalize login-form-title">Log In to see your contacts</h1>
            <form method="POST" id="login-form">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-default">Log In</button>
            </form>
            <br>
            <br>
            <span class="form-error text-danger"></span>
        </div>
        <div class="col-xs-2"></div>
    </div>
</div>
<script type="text/javascript" src="../js/functions.js"></script>
<script type="text/javascript" src="../js/login.js"></script>
<?php require_once("partials/footer.php"); ?>
