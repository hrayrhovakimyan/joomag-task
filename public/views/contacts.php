<?php
require_once("partials/head.php");
?>
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<script type="text/javascript" src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../lib/jquery.form.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <header>
        <a href="/logout" class="pull-right logout-link">Log out</a>
    </header>
    <div class="clearfix"></div>
    <div class="container">
        <div class="row">
            <h1 class="text-center" id="table-header">Contact List</h1>
            <table id="contacts-list" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Phone</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($contacts as $contact): ?>
                        <tr>
                            <td><?php echo $contact['firstname']; ?></td>
                            <td><?php echo $contact['lastname']; ?></td>
                            <td><?php echo $contact['phone']; ?></td>
                            <td><?php echo $contact['email']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <form method="POST" enctype="multipart/form-data" id="csv-form">
                <button class="btn btn-primary import-btn">Import From CSV</button>
                <input type="file" id="csv_file" name="csv_file" accept=".csv">
                <span class="text-danger form-error"></span>
            </form>
            <div class="page-load-spinner"></div>
    </div>
</div>
<script type="text/javascript" src="../js/contacts.js"></script>
<?php require_once "partials/footer.php"; ?>

